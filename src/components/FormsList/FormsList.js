import React , {Component} from 'react';
import './FormsList.css';
import axios from 'axios';

export default class FormList extends Component{
    constructor(props){
      super(props);
      this.state = {
          forms: [],
          tableClassName :  "container-fluide cont h-100 d-none",
          alertClassName : "alert alert-warning alert-dismissible fade ",
          alertMessageEnum : 0
      }
      this.handleAlertChange = this.handleAlertChange.bind(this);
    }

    handleAlertChange() {
        let alertMessage = "";
        let alertHader = "";
        if(this.state.alertMessageEnum === 0){
            alertMessage =  "you have no forms, please add forms in Form Builder Page";
            alertHader = "Pay Attention!"
        } else if(this.state.alertMessageEnum === 1){
            alertMessage =  "It's seems that the connection with the server is not working, please refresh the page and try again"
            alertHader = "Ho No!"
        }
        return <div className = {this.state.alertClassName} role = "alert">
                <button type = "button" className = "close" data-dismiss = "alert">&times;</button>
                <h3>{alertHader}</h3> {alertMessage}
         </div>
    }

    //save all the forms from the database
    componentWillMount(){
        axios.get('http://localhost:8080/forms')
        .then((res) => {
            let formsArray = res.data;
            let formMap = formsArray.map((currMap) => {return {id : currMap["formId"], name : currMap["formName"], submissions : currMap["submissions"]}});
            if(formMap.length === 0)
                this.setState({alertClassName : "alert alert-warning alert-dismissible fade show", alertMessageEnum:0});  
            else
                this.setState({tableClassName : "container-fluide cont h-100 "});
            this.setState( {forms : formMap} );
        })
        .catch(()=> {// No connection case
            this.setState({alertClassName : "alert alert-danger alert-dismissible fade show", alertMessageEnum:1}); 
        });
    }

    // Display all the rows in the table according to the information from the state
    displayRows=()=>{
        const currUrl = window.location.href;
        return this.state.forms.map((form) => {
            let addressSubmit = `${currUrl}submit/${form.id}`;
            let addressSubmissions = `${currUrl}submissions/${form.id}`;
            return <tr key = {form.id}>
                        <th className = "text-center" scope="row">{form.id}</th>
                        <td className = "text-center">{form.name}</td>
                        <td className = "text-center">{form.submissions}</td>
                        <td className = "text-center"><a href = {addressSubmit}>Submit</a></td>
                        <td className = "text-center"><a href = {addressSubmissions}>Submissions</a></td>
                    </tr>});
    };

    displayHeaders = () =>(
        <tr>
            <th className = "text-center" scope="col">Form Id</th>
            <th className = "text-center" scope="col">Form Name</th>
            <th className = "text-center" scope="col"># Submmisions</th>
            <th className = "text-center" scope="col">Submit Page</th>
            <th className = "text-center" scope="col">Submissions Page</th>
        </tr>);



    render(){
        return (
            <div className = "FormsList">
                {this.handleAlertChange()}
                <div className ={this.state.tableClassName}>
                    <div className = "row my-row h-100 w-100 justify-content-center mt-0">
                        <div className = "col-10 h-100 align-self-center w-100">
                            <div className = "table-responsive border ">
                                <table className ="table FormsTable table-hover w-100 h-100">
                                    <thead>
                                        {this.displayHeaders()}
                                    </thead>
                                    <tbody>
                                        {this.displayRows()}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


