import React ,{Component}from 'react';
import ButtonInput from '../ButtonInput/ButtonInput'
import './FormBuilder.css'

export default class FormBuilder extends Component{
    constructor(props){
        super(props);
        this.state = {
            currForm : [],
            currTypeField : "Choose Type",
            labelVal : "",
            fieldNameVal : "",
            formNameVal : "" ,
            fieldTypeClassName : "btn btn-default dropdown-toggle",
            submitFormClassName : "form-control",
            displeyFormHeader: "",
            comment : ""
        };
        this.handleChangeLabel = this.handleChangeLabel.bind(this);
        this.handleChangeFieldName = this.handleChangeFieldName.bind(this);
        this.handleChangeType = this.handleChangeType.bind(this);
        this.handleChangeFormName = this.handleChangeFormName.bind(this);
    }

    handleChangeFormName(event) {
        let formNameVal = event.target.value;
        this.setState({formNameVal : formNameVal});
        (formNameVal === "" && this.state.currForm.length !== 0) ?
            this.setState({formNameVal : formNameVal, displeyFormHeader : "Current Form"}):
        (formNameVal !== "" && this.state.currForm.length !== 0) ?
            this.setState({formNameVal : formNameVal, displeyFormHeader : formNameVal}):
            this.setState({formNameVal : formNameVal});      
    }

    handleChangeType(event) {
        this.setState({currTypeField: event.target.innerHTML});
    }

    handleChangeLabel(event) {
        this.setState({labelVal: event.target.value});
    }

    handleChangeFieldName(event) {
        this.setState({fieldNameVal: event.target.value});
    }


    //cases to reset the fields
    resetFields = (text) => {
        switch(text){
            case "endAddField" : // end of add field
                this.setState({fieldTypeClassName : "btn btn-default dropdown-toggle"});
                this.setState({labelVal : "", fieldNameVal : "", currTypeField : "Choose Type", comment : ""});
                break;
            case "beginningAddField" : // beginning of add field
            this.setState({comment: "", submitFormClassName : "form-control"});
                break;
            case "beginningAddForm": //beginning of add form
                this.setState({fieldTypeClassName : "btn btn-default dropdown-toggle" ,submitFormClassName : "form-control" , comment : ""});
                break;
            default:
        }
    }

    // A function that occurs when you press the "Add form", retains all the relevant information in the database
    AddForm = (e) => {
        this.resetFields("beginningAddForm");
        if(this.state.currForm.length === 0){
            this.setState({comment : "Please add at least one field"});
            return;
        }
        const formName = this.state.formNameVal;
        if(formName.length <= 1){
            this.setState({comment :  "a name for the form must have at least 2 letters" , submitFormClassName : "form-control border border-danger"});
            return;
        }
        fetch('http://localhost:8080/forms', {
                method:'POST', 
                headers:{'Content-Type':'application/json', 'Accept':'application/json'},
                body : JSON.stringify({
                    "formName" : formName,
                    "submissions" : 0,
                    "fields" : this.state.currForm,
                "submissionsText" : JSON.stringify( [] )})
        })
        .then((res) => { 
           if( res.status === 201){//CREATED status
                let currUrl = window.location.href;
                let addressSubmissions = currUrl.replace("builder", "");
                window.open(
                    addressSubmissions,
                    '_self'
                );
           }else{
            this.setState({comment :  "We are sorry, somehing went wrong, please try again"});
           }
        });
    }

    // A function that occurs when you click "Add Field", saves the new field to an array called currForm
    AddField = () => {
        this.resetFields("beginningAddField");
        let currField = {label : "", name : "", type : ""};
        currField.label =  this.state.labelVal;
        currField.name =  this.state.fieldNameVal;
        currField.type = this.state.currTypeField;
        if(this.state.currTypeField === "Choose Type"){
            this.setState({comment : "Please provide input type for the field"});
            this.setState({fieldTypeClassName : "btn btn-default dropdown-toggle border border-danger"});
            return;
        }

        let newArr = this.state.currForm.concat( [currField] );
        this.setState( {currForm : newArr, displeyFormHeader : "Current Form" }, this.resetFields("endAddField"));

    }
    
    //change the button text and save the type in state
    saveType = (type,e) => {
        this.setState( {currTypeField : type} );
        
    }

    //Displays the page where you can create a form
    render(){
        return (
            <div className= "row mt-5">
                <div className = "col-6 ml-2">
                    <form id = "myForm" >
                        <h1>New field information:</h1>
                        <ButtonInput label = "Field Label" name = "Field Label" placeHolder = "Please Enter" id = "FieldLabel" type = "text" handleChange= {this.handleChangeLabel} value = {this.state.labelVal}/>
                        <ButtonInput label = "Field Name" name = "Field Name" placeHolder = "Please Enter" id = "FieldName" type = "text" handleChange= {this.handleChangeFieldName} value = {this.state.fieldNameVal}/>
                        <div className ="dropdown dropright">
                            <button className = {this.state.fieldTypeClassName} type = "button"  data-toggle = "dropdown" style = {{ width : "120px" }}>
                                {this.state.currTypeField}
                                <span className = "caret"></span>
                            </button>
                            <div className = "dropdown-menu">
                                <a className = "dropdown-item" onClick = {this.handleChangeType} value = "text">text</a>
                                <a className = "dropdown-item" onClick = {this.handleChangeType} value = "color">color</a>
                                <a className = "dropdown-item" onClick = {this.handleChangeType} value = "date">date</a>
                                <a className = "dropdown-item" onClick = {this.handleChangeType} value = "email">email</a>
                                <a className = "dropdown-item" onClick = {this.handleChangeType} value = "tel">tel</a>
                                <a className = "dropdown-item" onClick = {this.handleChangeType} value = "number">number</a>
                            </div>
                        </div>
                        {/* A button to add a field to the fields list */}
                        <button type = "button" className = "btn btn-outline-primary  mt-3" onClick = {this.AddField} > Add Field </button>
                        {/* Button to save the current form */}
                        <div className = "form-group">
                            <div className = "input-group mt-5">
                                <input type = "text"  className = {this.state.submitFormClassName} placeholder = "Enter form name" onChange = {this.handleChangeFormName} value = {this.state.formNameVal}/>
                                <div className = "input-group-append">
                                    <button className = "btn btn-primary " type = "button" onClick = {this.AddForm} > Save Form </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <small className = "text-danger">{this.state.comment}</small>
                </div>
                {/* Displays each new field added to the current form */}
                <div className = "col-5 displayButtons">
                    <h1>{this.state.displeyFormHeader}</h1>
                    {this.state.currForm.map((field, fieldIndx) => <ButtonInput name = {field.name} label = {field.label} type ={field.type} id = {field.name} key = {fieldIndx + "fieldIndx"}/>)}
                </div>
            </div>
        )}
}
