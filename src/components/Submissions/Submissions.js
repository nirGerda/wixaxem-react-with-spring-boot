import React ,{Component} from 'react';
import axios from 'axios';

export default class Submissions extends Component {
    constructor(props){
        super(props);
        this.state = {
            formId : -1,
            subms : [],
            formName : "",
            headers : []
        };
    }

    goToMainPage = (res) => {
        const currUrl = window.location.href;
        let indx = currUrl.indexOf("submissions");
        let addressSubmissions = currUrl.substr(0,indx);
        window.open(
            addressSubmissions,
            '_self'
        );
    }

    // Save data: formId, formName and form's submmisions 
    componentWillMount(){
        const currId = this.props.match.params.id;
        this.setState( {formId:currId} )
        axios.get(`http://localhost:8080/forms/${currId}`)
        .then((res) => {  
            //no such form
            if(res.data === ""){
                this.goToMainPage(res);
            }else{
                //save formName and form's submissions
                
                let submArray = JSON.parse(res.data["submissionsText"]);
                let currFormName = res.data["formName"];
                this.setState({formName : currFormName, subms : submArray})
                if(submArray !== []) {
                    //save names of headers in state
                    axios.get(`http://localhost:8080/fields/${currId}`)
                    .then((res) => {
                        let formHeaders = {};
                        let fields = res.data;
                        //example for field: {"fieldId":1,"label":"label 1","name":"name1","type":"color","formId":1}
                        fields.forEach(field => {
                            formHeaders[ field["fieldId"] ] = field["name"];
                        });
                        this.setState( {headers: formHeaders} );
                    });
                }
            }  
        })
    };

    //create headers for table
    DisplayTableHeaders = () => {
        let thArr = [<th scope="col" className = "text-center" key = "uniqe1">Id</th>];
        let headers = this.state.headers;
        Object.keys(headers).forEach((headerId,headerIndx) => {
            thArr.push(<th scope = "col" className = "text-center" key = {headerIndx+"headerKey"}> {headers[headerId] }</th>)
        })
        return <thead className="thead-light"><tr>{thArr}</tr></thead>;
    }


    //Displays all current submissions for the form
    DisplayTableRows = () => {
        let row =[];
        let counter = 1;
        this.state.subms.map((sub,indexSub)=>{
            let columns =[<th scope="row" className = "text-center" key = "unique2">{counter}</th>];
            counter++;
            //Object.keys(sub) --> return array of keys
            Object.keys(sub).forEach((entityKey, indexKey) => {
                return columns.push(
                    <td className = "text-center" key = {indexKey + "entityKey" }>{sub[entityKey]}</td>
                )}   
            );
            return row.push(<tr key = {indexSub+"rowkey"}>{columns}</tr>);
        });
        return <tbody>{row}</tbody>;
    };

    getUrlSubmit = () => {
        const currUrl = window.location.href;
        let addressSubmit = currUrl.replace("submissions","submit");
        window.open(
            addressSubmit,
            '_self'
          );
    }

    render(){
        return(
            <div className = "container-fluid">
                <div className = "row ">
                    <div className = "col-md-2 col-sm-1 col-xs-1 p-0">
                        <button className = "btn btn-secondary btn-large  w-100 " id = "submitButton" onClick = {this.getUrlSubmit}>Go To Submit</button>
                    </div>
                    <div className="col-6 offset-1">
                        <h1 className = "text-center" style = {{fontFamily: "Times New Roman"}}><b>{this.state.formName}</b></h1>
                    </div> 
                </div>
                <div className="row justify-content-center">
                    <div className = "col">
                        <div className = "table-responsive border ">
                            <table className = "table FormsTable  table-hover w-100 h-100">
                                {this.DisplayTableHeaders()}
                                {this.DisplayTableRows()}
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}




