import React from 'react';
import './Menu.css';
import NavbarItem from './navbar-item/NavbarItem';
import {Route, BrowserRouter } from 'react-router-dom';
import FormsList from '../FormsList/FormsList';
import FormBuilder from '../FormBuilder/FormBuilder'
import Submit from '../Submit/Submit'
import Submissions from '../Submissions/Submissions'

/*component to manage all the pages in the website*/ 
const menu = (props) => {
  return (
    <BrowserRouter>
      <div className = "onlyRouterChild">
        <ul className = "navbar navbar-nav bg-dark navbar-expand-sm "> 
          <img className = "deadpool-img" alt = "icon" src = "https://www.freeiconspng.com/uploads/deadpool-symbol-png-0.png"></img>
          <NavbarItem name = "Forms List" url = "/"/>
          <NavbarItem name = "Form Builder" url = "/builder"/>
          <NavbarItem name = "" url = "/submit"/>
          <NavbarItem name = "" url = "/submissions"/>
        </ul>
        <div className = "content">
          <Route exact path = "/" component = {FormsList}/>
          <Route path = "/builder" component = {FormBuilder}/>
          <Route path = "/submit/:id" component = {Submit}/>
          <Route path = "/submissions/:id" component = {Submissions}/>
        </div>
      </div>
    </BrowserRouter>
  );
};
export default menu;