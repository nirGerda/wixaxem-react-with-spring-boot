import React from 'react';
import './NavbarItem.css';
import { NavLink } from 'react-router-dom';

const navbarItem = (props) => {
  return (
      <li className = "navbar-item singleItem">
        <NavLink to = {props.url}>{props.name}</NavLink>
      </li>
  );
};
export default navbarItem;