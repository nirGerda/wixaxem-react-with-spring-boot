import React from 'react';

const displayForm = (props) => {

    return (
        <div >
            <h1 style = {{fontFamily: "Times New Roman"}}><b>{props.formName}</b></h1>
            <form>
                {/* Displays the fields of the current form */}
                {props.displayFunc()}
                <small className = "text-danger" >{props.comment}</small>
                <button className = "btn btn-primary" type = "button" data-toggle = "modal" data-target = "#myModal" onClick = {props.submit}>Submit</button>
            </form>
        </div>
    );
}
export default displayForm;