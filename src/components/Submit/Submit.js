import React ,{Component} from 'react';
import ButtonInput from '../ButtonInput/ButtonInput'
import axios from 'axios';
import DisplayForm from '../DisplayForm/DisplayForm'

export default class Submit extends Component{
    constructor(props){
        super(props);
        this.state = {
            fields: [],
            formName: "",
            formId:-1,
            modalHeader : "",
            modalText : "",
            comment :"" 
        };
        this.submit = this.submit.bind(this);
    }
    
    goToMainPage = (res) => {
        const currUrl = window.location.href;
        let indx = currUrl.indexOf("submit");
        let addressSubmissions = currUrl.substr(0,indx);
        window.open(
            addressSubmissions,
            '_self'
        );
    }
    // Save data: formId, formName fields
    componentWillMount(){
        // Taking the formId from the path ("match" have path's params)
        const currId = this.props.match.params.id;
        this.setState( {formId : currId} )
        // Receiving the form fields from the database
         axios.get(`http://localhost:8080/fields/${currId}`)
        .then((res) => {
            if(res.data.length === 0){
                this.goToMainPage(res);
            }else{
                let currFields = res.data;
                // Transforming them from a set of arrays into a set of maps
                this.setState( {fields : currFields} )
                // Save the name of the form
                axios.get(`http://localhost:8080/forms/${currId}`,{params:{id:currId}})
                .then((res) => {
                    let currFormName = res.data["formName"];
                    this.setState({formName : currFormName})
                });
            }
        }
    )};
    
    displayFormFields = () => {
        return this.state.fields.map((field) => 
            (<div key ={field.fieldId} className = "mt-0 mb-3"> <ButtonInput name = {field.name} type = {field.type} id = {field.fieldId} label = {field.label} />
            <small id = {`forComments${field.fieldId}`} style = {{color:"red"}}></small></div>))
    }

    resetRedBoxes = () => {
        this.setState({modalHeader : "", modalText : ""})
        this.state.fields.forEach( (field) => {
            document.getElementById(field.fieldId).style.border = "";
            document.getElementById(`forComments${field.fieldId}`).innerText = "";
        })
    }

    isValidDate = (dateString) => {
        /*template from web */
        return /^\d{4}-\d{2}-\d{2}$/.test(dateString);
    }

    validateEmail = (email) => {
        /*template from web */
        const re = /([a-zA-z0-9]+)@((([^.]+)\.)+)([a-zA-Z]{1,}|[a-zA-Z.]{5,})/;
        return re.test(email);

    }

    checkFields = () => {
        let flagIsValid = true;
        this.resetRedBoxes();
        this.state.fields.forEach((field) => {
            let fId = field.fieldId;
            let fType =  field.type;
            let input = document.getElementById(fId);
            switch(fType){
                case "date":
                    if(!this.isValidDate(input.value)){
                        input.style.border = "solid 1px red";
                        document.getElementById(`forComments${fId}`).innerText = "Please insert legal date"
                        flagIsValid = false;
                    } 
                    break;
                case "email":
                    if(!this.validateEmail(input.value)){
                        input.style.border = "solid 1px red";
                        document.getElementById(`forComments${fId}`).innerText = "Please insert legal email"
                        flagIsValid = false;
                    }
                    break;
                default :
                    break;
            } 
        });
        return flagIsValid;
    }

    submit=(e)=>{  
        /*check validation */
        if(!this.checkFields()) {
            this.setState({modalHeader : "Pay Attention", modalText : "Please check your answers"}) 
            return;
        }      
        // a new map to be saved as a submission
        let myJson={};
        // collect all  information from the fields
        this.state.fields.map( (field)=>{
            let val = document.getElementById( (field.fieldId)).value;
            myJson[field.fieldId+""] = val;
            return field;
        });
     
        // gett current submissions of the same form
        axios.get(`http://localhost:8080/forms/${this.state.formId}`)
        .then((res)=>{
            // the field describing the previous entries in JSON format
            let existSubs = JSON.parse(res.data["submissionsText"]);
            existSubs = existSubs.concat([myJson]);
            let numSubs = existSubs.length;
            //add the current submission to previous submissions
            fetch('http://localhost:8080/forms', {
            method:'PUT', 
            headers:{'Content-Type':'application/json', 'Accept':'application/json'},
            body : JSON.stringify({
                "form_id" :this.state.formId,
                "str":JSON.stringify(existSubs),
                "submissions": numSubs
                })
            }) 
            .then((response)=> {
                this.setState({comment : ""});
                if(response.status === 200){
                    this.resetRedBoxes();
                    this.setState({modalHeader : "congratulations!",modalText : "Your submission has been successfully saved"});
                }else{
                    this.setState({comment : "We are sorry, something went terribly wrong, please try again"});
                }}) 
        }) 
     
    }

    getUrlSubmissions = ()=>{
        const currUrl = window.location.href;
        let addressSubmissions = currUrl.replace("submit", "submissions");
        window.open(
            addressSubmissions,
            '_self'
          );
    }

    render(){
        return(
            <div className = "container-fluid">
                <div className = "row ">
                    <div className = "col-3 text-left p-0">
                        <button className = "btn btn-secondary  w-50 " id = "submissionsButton" onClick = {this.getUrlSubmissions}>Go To Submissions</button>
                    </div>
                    <div className = "col col-6">
                        <DisplayForm displayFunc = {this.displayFormFields} comment = {this.state.comment} formName = {this.state.formName} submit = {this.submit} ></DisplayForm>
                        <div id = "myModal" className = "modal fade hide" role = "dialog">
                            <div className = "modal-dialog">
                                <div className = "modal-content">
                                    <div className = "modal-header">
                                        <h4 className = "modal-title text-left">{this.state.modalHeader}</h4>
                                    </div>
                                    <div className = "modal-body">
                                        <p> {this.state.modalText}</p>
                                    </div>
                                    <div className = "modal-footer">
                                        <button type = "button" className = "btn btn-primary" data-dismiss = "modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}




