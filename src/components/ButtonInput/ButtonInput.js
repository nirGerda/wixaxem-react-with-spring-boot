import React from 'react';

/*Component for creating button with label*/ 
const buttonInput = (props) => {
    return (
        <div className = "form-group mb-2">
            <div className = "input-group">
                 <span className = "input-group-text text-center" style = {{width : "104px"}}> {props.label} </span>
                <input type = {props.type}  className = "form-control " placeholder = {props.placeHolder} id = {props.id} name = {props.name} style = {{height : "38px"}}  value = {props.value} onChange = {props.handleChange}/>
            </div>
        </div>  
    );
};
export default buttonInput;