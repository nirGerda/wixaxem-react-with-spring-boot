package io.javabrains;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FormsProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(FormsProjectApplication.class, args);
		System.out.println("server is ready");
	}
}
