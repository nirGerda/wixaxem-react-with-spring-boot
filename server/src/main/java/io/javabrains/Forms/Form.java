package io.javabrains.Forms;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Form {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) // INTEGER PRIMARY KEY AUTOINCREMENT
	private int formId;
	@NotNull(message = "Name cannot be empty")
	@Size(min = 2, max = 50, message = "Form name must be 2-50 letters")
	private String formName;
	@Min(0)
	private int submissions;// INT,
	private String submissionsText;// TEXT

	public Form() {
	}

	public Form(String formName, int submissions, String submissionsText) {
		this.formName = formName;
		this.submissions = submissions;
		this.submissionsText = submissionsText;
	}

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public int getSubmissions() {
		return submissions;
	}

	public void setSubmissions(int submissions) {
		this.submissions = submissions;
	}

	public String getSubmissionsText() {
		return submissionsText;
	}

	public void setSubmissionsText(String submissionsText) {
		this.submissionsText = submissionsText;
	}

}
