package io.javabrains.Forms;

import org.springframework.data.repository.CrudRepository;

public interface FormRepository extends CrudRepository<Form, Integer> {

}
