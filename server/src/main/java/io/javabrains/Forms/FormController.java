package io.javabrains.Forms;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.javabrains.Fields.Field;
import io.javabrains.Fields.FieldService;

@RestController
@CrossOrigin
public class FormController {
	@Autowired
	private FormService formService;

	@Autowired
	private FieldService fieldService;

	@ResponseBody
	// add just form, without fields, and return his id
	public int addFormOld(@Valid Form form) {
		int newFormId = formService.addForm(form);
		return newFormId;
	}

	@RequestMapping("/forms")
	@ResponseBody

	public List<Form> getAllForms() {
		return formService.getAllForms();
	}

	@RequestMapping(method = RequestMethod.POST, value = "/forms")
	@ResponseBody
	public ResponseEntity addForm(@RequestBody Map<String, Object> body) {
		/// add the form
		int newFormId = addFormOld(
				new Form(body.get("formName").toString(), 0, body.get("submissionsText").toString()));
		// add the fields of the form
		@SuppressWarnings("unchecked")
		List<Map<String, Object>> fields = (List<Map<String, Object>>) body.get("fields");
		for (Map<String, Object> field : fields) {
			String label = (String) field.get("label");
			String name = (String) field.get("name");
			String type = (String) field.get("type");
			fieldService.addField(new Field(label, name, type, newFormId));
		}
		return new ResponseEntity(HttpStatus.CREATED);
	}

	@RequestMapping("/forms/{formId}")
	@ResponseBody
	public Form getFormById(@PathVariable int formId) {
		return formService.getFormById(formId);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/forms")
	@ResponseBody
	public ResponseEntity updateSubmissionsForm(@RequestBody Map<String, Object> body) {
		int formId = Integer.parseInt(body.get("form_id").toString());
		String newSubmissionText = body.get("str").toString();
		int submissions = Integer.parseInt(body.get("submissions").toString());
		Form currForm = formService.getFormById(formId);
		currForm.setSubmissions(submissions);
		currForm.setSubmissionsText(newSubmissionText);
		formService.addForm(currForm);
		return new ResponseEntity(HttpStatus.OK);
	}
}
