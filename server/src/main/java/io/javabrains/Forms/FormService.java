package io.javabrains.Forms;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FormService {

	@Autowired
	private FormRepository formRepository;

	public int addForm(Form form) {
		Form newForm = formRepository.save(form);
		return newForm.getFormId();
	}

	public List<Form> getAllForms() {
		List<Form> forms = new ArrayList<>();
		formRepository.findAll().forEach(forms::add);
		return forms;
	}

	public Form getFormById(int formId) {
		return formRepository.findOne(formId);
	}
}
