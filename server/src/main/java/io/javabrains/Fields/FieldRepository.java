package io.javabrains.Fields;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

public interface FieldRepository extends CrudRepository<Field, String> {
	public List<Field> findByFormId(int formId);
}
