package io.javabrains.Fields;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class FieldController {
	@Autowired
	private FieldService fieldService;

	@RequestMapping(method = RequestMethod.POST, value = "/fields")
	public void addForm(@RequestBody @Valid Field field) {
		fieldService.addField(field);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/fields/{formId}")
	public List<Field> getFieldsByFormId(@PathVariable int formId) {
		return fieldService.findByFormId(formId);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/fields")
	public List<Field> getAllFields() {
		return fieldService.getAllFields();
	}

}
