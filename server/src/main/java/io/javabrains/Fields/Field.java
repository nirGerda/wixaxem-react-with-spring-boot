package io.javabrains.Fields;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Field {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO) // INTEGER PRIMARY KEY AUTOINCREMENT
	private int fieldId;
	private String label;
	private String name;
	@NotNull(message = "Error : Field must have type")
	@Size(min = 3, max = 8)
	private String type;
	@Min(0)
	private int formId;

	public Field() {
	}

	public Field(String label, String name, String type, int formId) {
		this.label = label;
		this.name = name;
		this.type = type;
		this.formId = formId;
	}

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public int getFieldId() {
		return fieldId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
