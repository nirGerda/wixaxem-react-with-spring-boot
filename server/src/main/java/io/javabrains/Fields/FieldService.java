package io.javabrains.Fields;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FieldService {
	@Autowired
	private FieldRepository fieldRepository;

	public void addField(Field field) {
		fieldRepository.save(field);
	}

	public List<Field> findByFormId(int formId) {
		return fieldRepository.findByFormId(formId);
	}

	public List<Field> getAllFields() {
		List<Field> fields = new ArrayList<>();
		fieldRepository.findAll().forEach(fields::add);
		return fields;
	}
}
