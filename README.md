﻿------------------------------------
instructions for running the program
------------------------------------
server:
	- make sure to have maven on your computer
	- open cmd in the project direction
	- go to "server" folder with :
		 "cd server"
	- run "mvn spring-boot:run"
	- when the server is ready, you will see "server is ready" printed to the screen
	
client:
	- open cmd in the project direction (wixaxem folder)
	- run the client with:
		npm start
	- it will open a new window in the main page 


------
notice
------	
- the client works with a server that is located only on the localhost (http://localhost:8080).